﻿using AgileMQ.Interfaces;
using System;
using System.Collections.Generic;
using DataModels = TemperatureService.Data.Models;
using TemperatureService.Directories.PervasiveBuilding.Temperature.Models;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using TemperatureService.Data;
using TemperatureService.Data.Repositories;
using TemperatureService.Data.Exceptions;
using TemperatureService.Utilities;
using AgileMQ.Extensions;
using TemperatureService.Directories.PervasiveBuilding.Room.Models;

namespace TemperatureService.Subscribers
{
   public class TemperatureSubscriber : IAgileSubscriber<Temperature>
   {
      public IAgileBus Bus { get; set; }

      public async Task<Temperature> GetAsync(Temperature model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Temperature temperature = await ctx.Temperatures.FindByIdAsync(model.Id.Value);

         if (temperature == null)
            throw new ObjectNotFoundException("Temperature not found, a wrong ID was used.");

         model = Mapper.Map(temperature);

         return (model);
      }

      public async Task<Temperature> PostAsync(Temperature model, MessageContainer container)
      {
         /*
         return (Task.Factory.StartNew(() =>
         {
            DataContext ctx = container.Resolve<DataContext>();

            //Get the indicated room
            Room room = new Room()
            {
               Id = model.RoomId,
            };
            room = Bus.GetAsync(room).Result;

            //Cretion of new temperature record
            DataModels.Temperature temperature = new DataModels.Temperature()
            {
               InsertionDate = DateTime.Now,
               HumidityValue = model.HumidityValue.Value,
               TemperatureValue = model.TemperatureValue.Value,
               RoomId = room.Id.Value,
            };

            //Add the new record to DB adn save changes
            ctx.Temperatures.Add(temperature);
            ctx.SaveChanges();

            model = Mapper.Map(temperature);

            return (model);
         }));   
         */

         DataContext ctx = container.Resolve<DataContext>();

         //Get the indicated room
         Room room = new Room()
         {
            Id = model.RoomId,
         };
         room = await Bus.GetAsync(room);

         //Cretion of new temperature record
         DataModels.Temperature temperature = new DataModels.Temperature()
         {
            InsertionDate = DateTime.Now,
            HumidityValue = model.HumidityValue.Value,
            TemperatureValue = model.TemperatureValue.Value,
            RoomId = room.Id.Value,
         };

         //Add the new record to DB adn save changes
         await ctx.Temperatures.AddAsync(temperature);
         await ctx.SaveChangesAsync();

         model = Mapper.Map(temperature);

         return (model);
      }

      public async Task DeleteAsync(Temperature model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Temperature temperature = await ctx.Temperatures.FindByIdAsync(model.Id.Value);

         if (temperature == null)
            throw new ObjectNotFoundException("Temperature not found, a wrong ID was used.");

         ctx.Temperatures.Remove(temperature);
         await ctx.SaveChangesAsync();
      }

      public async Task<List<Temperature>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         long? roomId = filter.Get<long?>("roomId");

         List<Temperature> list = new List<Temperature>();

         List<DataModels.Temperature> temperatureList = await ctx.Temperatures.GetListAsync(roomId);

         foreach (DataModels.Temperature temperature in temperatureList)
         {
            list.Add(Mapper.Map(temperature));
         }

         return (list);
      }

      public Task PutAsync(Temperature model, MessageContainer container)
      {
         throw new NotImplementedException();
      }

      public Task<Page<Temperature>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
      {
         throw new NotImplementedException();
      }
   }
}
