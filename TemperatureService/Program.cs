﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using System;
using TemperatureService.Data;
using TemperatureService.Logger;
using TemperatureService.Subscribers;

namespace TemperatureService
{
  public class Program
  {
    static void Main(string[] args)
    {
      using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=50;AppId=TemperatureService"))
      {
        bus.Logger = new ConsoleLogger();

        bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

        bus.Suscribe(new TemperatureSubscriber());

        Console.WriteLine("TemperatureService Ready!");
        Console.WriteLine();
        Console.ReadLine();
      }
    }
  }
}
