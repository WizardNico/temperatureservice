﻿using PervasiveTemperatureModels = TemperatureService.Directories.PervasiveBuilding.Temperature.Models;
using DataModels = TemperatureService.Data.Models;

namespace TemperatureService.Utilities
{
   public class Mapper
   {
      public static PervasiveTemperatureModels.Temperature Map(DataModels.Temperature temperature)
      {
         return new PervasiveTemperatureModels.Temperature()
         {
            Id = temperature.Id,
            InsertionDate = temperature.InsertionDate,
            TemperatureValue = temperature.TemperatureValue,
            HumidityValue = temperature.HumidityValue,
            RoomId = temperature.RoomId
         };
      }
   }
}
