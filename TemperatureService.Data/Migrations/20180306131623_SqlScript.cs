﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TemperatureService.Data.Migrations
{
   public partial class SqlScript : Migration
   {
      protected override void Up(MigrationBuilder migrationBuilder)
      {
         //Room 1
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 20, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 21, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 19, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 20, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 1, 22, '20180305 07:00:00 PM')");

         //Room 2
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 11, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 12, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (90, 2, 10, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 11, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 10, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 21, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (89, 2, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 2, 20, '20180305 07:00:00 PM')");

         //Room 3
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (80, 3, 20, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (90, 3, 20, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 3, 22, '20180305 07:00:00 PM')");

         //Room 4
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 15, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (71, 4, 15, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (71, 4, 15, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 15, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (80, 4, 15, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 4, 22, '20180305 07:00:00 PM')");

         //Room 5
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 15, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 15, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 15, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 15, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 15, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 5, 22, '20180305 07:00:00 PM')");

         //Room 6
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 16, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 15, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 17, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 14, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 15, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 22, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 23, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 6, 22, '20180305 07:00:00 PM')");

         //Room 7
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 15, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 15, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 15, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 15, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 15, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 7, 22, '20180305 07:00:00 PM')");

         //Room 8
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 8, 22, '20180305 07:00:00 PM')");

         //Room 9
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 15, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 15, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 15, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 15, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 15, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 20, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 9, 22, '20180305 07:00:00 PM')");

         //Room 10
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 10, 11, '20180301 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (70, 10, 12, '20180302 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (72, 10, 10, '20180303 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (66, 10, 11, '20180304 07:00:00 AM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (60, 10, 10, '20180305 07:00:00 AM')");

         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (40, 10, 21, '20180301 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (50, 10, 20, '20180302 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (60, 10, 20, '20180303 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (50, 10, 21, '20180304 07:00:00 PM')");
         migrationBuilder.Sql("INSERT INTO Temperatures (HumidityValue, RoomId, TemperatureValue, InsertionDate) VALUES (50, 10, 20, '20180305 07:00:00 PM')");
      }

      protected override void Down(MigrationBuilder migrationBuilder)
      {

      }
   }
}
