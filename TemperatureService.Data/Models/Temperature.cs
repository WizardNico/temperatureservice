﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TemperatureService.Data.Models
{
   public class Temperature
   {
      public long Id { get; set; }



      public DateTime InsertionDate { get; set; }

      public double TemperatureValue { get; set; }

      public double HumidityValue { get; set; }



      public long RoomId { get; set; }
   }
}
