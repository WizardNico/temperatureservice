﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemperatureService.Data.Models;

namespace TemperatureService.Data.Repositories
{
   public static class TemperatureRepository
   {
      public static async Task<Temperature> FindByIdAsync(this DbSet<Temperature> repository, long id)
      {
         Temperature temperature = await repository
           .Where(temp => temp.Id == id)
           .SingleOrDefaultAsync();

         return (temperature);
      }

      public static async Task<List<Temperature>> GetListAsync(this DbSet<Temperature> repository, long? roomId)
      {
         IQueryable<Temperature> query = repository;

         if (roomId != null)
         {
            query = query.Where(temp => temp.RoomId == roomId);
         }

         List<Temperature> result = await query
            .ToListAsync();

         return (result);
      }
   }
}
