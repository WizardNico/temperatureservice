﻿using Microsoft.EntityFrameworkCore;
using TemperatureService.Data.Models;

namespace TemperatureService.Data
{
   public class DataContext : DbContext
   {
      public DbSet<Temperature> Temperatures { get; set; }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=TemperatureService;Persist Security Info=True;User ID=sa;Password=nico93;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
      }
   }
}
