﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemperatureService.Data.Exceptions
{
  public class ObjectNotFoundException : Exception
  {
    public ObjectNotFoundException(string message) : base(message) { }
  }
}
