﻿using AgileMQ.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TemperatureService.Directories.PervasiveBuilding.Room.Shared;

namespace TemperatureService.Directories.PervasiveBuilding.Room.Models
{
   [QueuesConfig(Directory = "PervasiveBuilding", Subdirectory = "Room", ResponseEnabled = true)]
   public class Room
   {
      [Required]
      public long? Id { get; set; }



      [Required]
      public int? RoomNumber { get; set; }

      [Required]
      public int? FloorNumber { get; set; }



      [Required]
      public long? FireEscapeId { get; set; }

      public FireEscape FireEscape { get; set; }
   }
}
