﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TemperatureService.Directories.PervasiveBuilding.Employee.Enum
{
   public enum EmployeeRole
   {
      Programmer = 10,
      Architect = 20,
      ProjectManager = 30,
      SystemEngineer = 40,
      Security = 50,
   }
}
